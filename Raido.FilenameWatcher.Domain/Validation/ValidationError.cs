﻿using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.Domain.Validation
{
    public sealed class ValidationError
    {
        public string FolderLocation { get; internal set; }
        public string FileName { get; internal set; }
        public string Message { get; internal set; }
        public ResultTarget ResultTarget { get; internal set; }
        public override string ToString()
        {
            return $"FileName: {this.FileName} Message: {this.Message}";
        }
    }
}
