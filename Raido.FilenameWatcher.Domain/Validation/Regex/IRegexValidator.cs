﻿using System.Collections.Generic;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.Domain.Validation.Regex
{
    public interface IRegexValidator
    {
        IEnumerable<ValidationError> Validate(string pattern, string text, string folderLocation, ResultTarget resultTarget);
    }
}
