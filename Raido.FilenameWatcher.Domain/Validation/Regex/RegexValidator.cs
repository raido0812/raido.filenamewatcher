﻿using System;
using System.Collections.Generic;
using Raido.FilenameWatcher.RuleConfiguration.Enums;
using RegularExpressions = System.Text.RegularExpressions.Regex;

namespace Raido.FilenameWatcher.Domain.Validation.Regex
{
    internal sealed class RegexValidator : IRegexValidator
    {
        public IEnumerable<ValidationError> Validate(string pattern, string fileName, string folderLocation, ResultTarget resultTarget)
        {
            var errors = new List<ValidationError>();
            if (string.IsNullOrEmpty(pattern))
            {
                return errors;
            }

            try
            {
                if (!RegularExpressions.Match(fileName, pattern).Success)
                {
                    errors.Add(
                        new ValidationError
                        {
                            FolderLocation = folderLocation,
                            FileName = fileName,
                            Message = $"Does not match pattern: {pattern}",
                            ResultTarget = resultTarget,
                        });
                }
            }
            catch (ArgumentException)
            {
                return errors;
            }

            return errors;
        }
    }
}
