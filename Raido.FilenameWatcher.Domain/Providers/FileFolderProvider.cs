﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Raido.FilenameWatcher.Domain.Providers
{
    internal sealed class FileFolderProvider : IFileFolderProvider
    {
        #region Fields
        private readonly ILog _logger;
        #endregion

        #region Constructor
        public FileFolderProvider(ILog logger)
        {
            _logger = logger;
        }
        #endregion

        #region Methods
        public bool DirectoryExists(string directoryLocation)
        {
            bool result = Directory.Exists(directoryLocation);
            _logger?.Debug($"Directory {directoryLocation} {(result ? "Exists" : "Not Found")}");
            return result;
        }

        public IEnumerable<string> GetFilesInDirectory(string directoryLocation)
        {
            var directoryInfo = new DirectoryInfo(directoryLocation);
            var files = new List<string>();
            if (directoryInfo.Exists)
            {
                files = directoryInfo.GetFiles().Select(f => f.Name).ToList();
            }
            else
            {
                _logger?.Debug($"Directory {directoryLocation} Not Found. Unable to load files");
            }

            return files;
        }
        #endregion
    }
}
