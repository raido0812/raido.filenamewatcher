﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.Domain.Providers
{
    public interface IFileFolderProvider
    {
        bool DirectoryExists(string directoryLocation);

        IEnumerable<string> GetFilesInDirectory(string directoryLocation);
    }
}
