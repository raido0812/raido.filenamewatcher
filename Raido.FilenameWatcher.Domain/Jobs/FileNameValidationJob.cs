﻿using log4net;
using Quartz;
using Raido.FilenameWatcher.Domain.RulesEngine;
using Raido.FilenameWatcher.RuleConfiguration;

namespace Raido.FilenameWatcher.Domain.Jobs
{
    public class FileNameValidationJob : IJob
    {
        private readonly ILog _logger;
        private readonly IFolderProcessor _folderProcessor;
        private readonly RuleConfigurationSection _config;

        public FileNameValidationJob(
            ILog logger,
            IFolderProcessor folderProcessor,
            RuleConfigurationSection config)
        {
            _logger = logger;
            _folderProcessor = folderProcessor;
            _config = config;
        }

        public void Execute(IJobExecutionContext context)
        {
            _logger.Debug($"Starting Job {context.JobDetail.Description}");
            _folderProcessor.ProcessFoldersInConfig(_config);
            _logger.Debug($"Finished Job {context.JobDetail.Description}");
        }
    }
}
