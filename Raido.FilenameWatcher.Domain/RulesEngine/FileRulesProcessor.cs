﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using Raido.FilenameWatcher.Domain.Validation;
using Raido.FilenameWatcher.Domain.Validation.Regex;
using Raido.FilenameWatcher.RuleConfiguration;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.Domain.RulesEngine
{
    internal sealed class FileRulesProcessor : IRulesProcessor
    {
        #region Fields
        private readonly ILog _logger;
        private readonly IRegexValidator _regexValidator;
        private readonly IEnumerable<ValidationError> EmptyErrors = new List<ValidationError>();
        #endregion

        #region Constructor
        public FileRulesProcessor(
            ILog logger,
            IRegexValidator regexValidator)
        {
            _logger = logger;
            _regexValidator = regexValidator;
        }
        #endregion

        #region Methods
        public IEnumerable<ValidationError> ProcessFileNames(IEnumerable<string> fileNames, string folderLocation, RuleCollection rules, ResultTarget resultTarget)
        {
            var errors = new List<ValidationError>();
            _logger?.Info($"Processing Files: {string.Join(",", fileNames)}");
            foreach (var file in fileNames)
            {
                // Assuming all rules in root level are "And". if wanted "Or" could have specified
                errors.AddRange(ProcessAndRules(rules, file, folderLocation, resultTarget));
            }
            
            _logger?.Info($"Found: {errors.Count} Errors");
            return errors;
        }

        internal IEnumerable<ValidationError> ProcessAndRules(RuleCollection rules, string fileName, string folderLocation, ResultTarget resultTarget)
        {
            var validationErrors = new List<ValidationError>();
            foreach (Rule rule in rules)
            {
                validationErrors.AddRange(ProcessRule(rule, fileName, folderLocation, resultTarget));
            }

            return validationErrors;
        }

        internal IEnumerable<ValidationError> ProcessOrRules(RuleCollection rules, string fileName, string folderLocation, ResultTarget resultTarget)
        {
            var validationErrors = new List<ValidationError>();
            foreach (Rule rule in rules)
            {
                var ruleErrors = ProcessRule(rule, fileName, folderLocation, resultTarget).ToList();
                if (ruleErrors == null || ruleErrors.Count == 0)
                {
                    return EmptyErrors;
                }
                else
                {
                    validationErrors.AddRange(ruleErrors);
                }
            }

            return validationErrors;
        }

        internal IEnumerable<ValidationError> ProcessRule(Rule rule, string fileName, string folderLocation, ResultTarget resultTarget)
        {
            if (rule.Type == ElementType.Regex)
            {
                return _regexValidator?.Validate(rule.RuleValue, fileName, folderLocation, resultTarget) ?? EmptyErrors;
            }
            else if (rule.Type == ElementType.And &&
                    rule.Rules != null && rule.Rules.Count > 0)
            {
                return ProcessAndRules(rule.Rules, fileName, folderLocation, resultTarget);
            }
            else if (rule.Type == ElementType.Or &&
                    rule.Rules != null && rule.Rules.Count > 0)
            {
                return ProcessOrRules(rule.Rules, fileName, folderLocation, resultTarget);
            }

            return EmptyErrors;
        }
        #endregion
    }
}
