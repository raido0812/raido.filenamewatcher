﻿using System.Collections.Generic;
using Raido.FilenameWatcher.Domain.Validation;
using Raido.FilenameWatcher.RuleConfiguration;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.Domain.RulesEngine
{
    public interface IRulesProcessor
    {
        IEnumerable<ValidationError> ProcessFileNames(IEnumerable<string> fileNames, string folderLocation, RuleCollection rules, ResultTarget resultTarget);
    }
}
