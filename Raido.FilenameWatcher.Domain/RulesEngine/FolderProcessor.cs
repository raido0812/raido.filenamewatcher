﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Raido.FilenameWatcher.Domain.Mailer;
using Raido.FilenameWatcher.Domain.Providers;
using Raido.FilenameWatcher.Domain.TextTemplating;
using Raido.FilenameWatcher.Domain.Validation;
using Raido.FilenameWatcher.RuleConfiguration;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.Domain.RulesEngine
{
    internal sealed class FolderProcessor : IFolderProcessor
    {
        #region Fields
        private readonly ILog _logger;
        private readonly IEmailer _mailer;
        private readonly IFileFolderProvider _fileFolderProvider;
        private readonly IRulesProcessor _rulesProcessor;
        #endregion

        #region Constructor
        public FolderProcessor(
            ILog logger,
            IEmailer mailer,
            IFileFolderProvider fileFolderProvider,
            IRulesProcessor rulesProcessor)
        {
            _logger = logger;
            _mailer = mailer;
            _fileFolderProvider = fileFolderProvider;
            _rulesProcessor = rulesProcessor;
        }
        #endregion

        #region Methods
        public async Task ProcessFoldersInConfig(RuleConfigurationSection config)
        {
            var errors = GetValidationErrors(config.Folders);
            LogErrors(errors);
            await SendErrorsToEmail(config, errors);
        }

        private IEnumerable<ValidationError> GetValidationErrors(FolderCollection folderCollection)
        {
            _logger?.Debug("Processing folders in config");
            var errors = new List<ValidationError>();

            foreach (Folder folder in folderCollection)
            {
                // Get files
                if (_fileFolderProvider != null && _fileFolderProvider.DirectoryExists(folder.Location))
                {
                    // Process all rules on file
                    var fileNames = _fileFolderProvider.GetFilesInDirectory(folder.Location);
                    errors.AddRange(
                        _rulesProcessor?
                            .ProcessFileNames(
                                fileNames, 
                                folder.Location, 
                                folder.Rules, 
                                folder.ResultTarget));
                }
            }

            return errors;
        }

        private void LogErrors(IEnumerable<ValidationError> errors)
        {
            if (_logger == null)
            {
                return;
            }

            // Output Validation Errors if Any
            foreach (var error in errors.Where(ve => ve.ResultTarget == ResultTarget.Log).ToList())
            {
                _logger.Info(error.ToString());
            }
        }

        private async Task SendErrorsToEmail(RuleConfigurationSection config, IEnumerable<ValidationError> errors)
        {
            // if no mailer configured, don't even bother with emailing the items
            if (_mailer == null ||
                string.IsNullOrWhiteSpace(config.EmailSubject) ||
                string.IsNullOrWhiteSpace(config.ToAddress))
            {
                return;
            }

            _logger?.Debug("Emailing Errors");
            var validationErrorsToEmail = errors
                .Where(ve => ve.ResultTarget == ResultTarget.Email)
                .GroupBy(ve => ve.FolderLocation)
                .ToDictionary(g => g.Key, g => g.Select(ve => ve));
            // Send Email instead of logging if specified
            if (validationErrorsToEmail != null &&
                validationErrorsToEmail.Any())
            {
                // Generate Template and Send
                var template = new FileNameValidationResultTemplate(validationErrorsToEmail);
                string emailBody = template.TransformText();
                await _mailer.SendEmail(emailBody, config.EmailSubject, config.ToAddress);
            }
        }
        #endregion
    }
}
