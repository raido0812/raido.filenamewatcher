﻿using System.Threading.Tasks;
using Raido.FilenameWatcher.RuleConfiguration;

namespace Raido.FilenameWatcher.Domain.RulesEngine
{
    public interface IFolderProcessor
    {
        Task ProcessFoldersInConfig(RuleConfigurationSection config);
    }
}
