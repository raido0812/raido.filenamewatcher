﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Raido.FilenameWatcher.Domain.Mailer
{
    internal class Emailer : IEmailer
    {
        #region Fields
        private readonly ILog _logger;
        #endregion

        #region Constructor
        public Emailer(ILog logger)
        {
            _logger = logger;
        }
        #endregion

        #region Method
        public async Task<bool> SendEmail(string message, string subject, string toAddress)
        {
            return await SendEmail(message, subject, new List<string> { toAddress });
        }

        public async Task<bool> SendEmail(string message, string subject, IEnumerable<string> toAddress)
        {
            bool success = true;

            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Clear();
            foreach (var address in toAddress)
            {
                mailMsg.To.Add(address);
            }

            mailMsg.CC.Clear();
            mailMsg.Bcc.Clear();
            mailMsg.Subject = subject;
            mailMsg.BodyEncoding = Encoding.Unicode;
            mailMsg.IsBodyHtml = true;
            mailMsg.Body = message;
            mailMsg.Priority = MailPriority.Normal;

            SmtpClient smtpMail = new SmtpClient();
            smtpMail.EnableSsl = true;
            try
            {
                await smtpMail.SendMailAsync(mailMsg);

            }
            catch (SmtpException smtpEx)
            {
                _logger.Error("Send Mail Error", smtpEx);
                success = false;
            }
            catch (Exception ex)
            {
                _logger.Error("Send Mail Error", ex);
                success = false;

                throw;
            }

            return success;
        }
        #endregion
    }
}
