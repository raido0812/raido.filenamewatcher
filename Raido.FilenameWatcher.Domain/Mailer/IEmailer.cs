﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.Domain.Mailer
{
    public interface IEmailer
    {
        Task<bool> SendEmail(string message, string subject, string toAddress);
        Task<bool> SendEmail(string message, string subject, IEnumerable<string> toAddress);
    }
}
