﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raido.FilenameWatcher.Domain.Validation;

namespace Raido.FilenameWatcher.Domain.TextTemplating
{
    public partial class FileNameValidationResultTemplate
    {
        private readonly IDictionary<string, IEnumerable<ValidationError>> ValidationResults;

        public FileNameValidationResultTemplate(IDictionary<string, IEnumerable<ValidationError>> validationResults)
        {
            ValidationResults = validationResults;
        }
    }
}
