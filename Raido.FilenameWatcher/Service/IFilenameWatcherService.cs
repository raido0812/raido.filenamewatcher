﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.Service
{
    internal interface IFilenameWatcherService
    {
        bool Start();

        bool Stop();
    }
}
