﻿using System.Timers;
using log4net;
using Quartz;
using Raido.FilenameWatcher.Domain.RulesEngine;
using Raido.FilenameWatcher.RuleConfiguration;
using Threading = System.Threading;

namespace Raido.FilenameWatcher.Service
{
    internal sealed class FilenameWatcherService : IFilenameWatcherService
    {
        private readonly ILog _logger;
        private readonly IScheduler _scheduler;
        public FilenameWatcherService(
            ILog logger,
            IScheduler scheduler)
        {
            _logger = logger;
            _scheduler = scheduler;
        }

        public bool Start()
        {
            if (!_scheduler.IsStarted)
            {
                _logger?.Info("Scheduler Starting");
                _scheduler.Start();
            }

            _logger?.Info("Scheduler Started");
            return true;
        }

        public bool Stop()
        {
            _logger?.Info("Scheduler Shutting Down");
            _scheduler.Shutdown(true);
            _logger?.Info("Service Stopped");
            return true;
        }
    }
}
