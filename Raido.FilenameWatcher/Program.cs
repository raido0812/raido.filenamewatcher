﻿using Quartz;
using Topshelf.ServiceConfigurators;
using Autofac;
using Autofac.Extras.Quartz;
using Raido.FilenameWatcher.Service;
using Topshelf;
using Topshelf.Autofac;
using Topshelf.Quartz;
using Raido.FilenameWatcher.Domain.Jobs;
using log4net;
using Raido.FilenameWatcher.RuleConfiguration;
using System;

namespace Raido.FilenameWatcher
{
    public class Program
    {
        static void Main(string[] args)
        {
            var validationSchedule = RuleConfigurationSection.GetConfig().ScheduleCron;
            var container = IoCBinding.GetIoCContainer();
            ScheduleJobServiceConfiguratorExtensions.SchedulerFactory = () => container.Resolve<IScheduler>();

            HostFactory.Run(hostConfig =>
            {
                hostConfig.SetDescription("Watches configured folders, validates filenames in the folder and sends report of files violating the rules");
                hostConfig.SetDisplayName("Raido Filename Watcher");
                hostConfig.SetServiceName("Raido Filename Watcher");
                hostConfig.UseAutofacContainer(container);

                hostConfig.Service<IFilenameWatcherService>(service =>
                {
                    service.ConstructUsingAutofacContainer();
                    service.WhenStarted(tc => tc.Start());
                    service.WhenStopped(tc => tc.Stop());

                    service.ScheduleQuartzJob(q =>
                    {
                        q.WithJob(JobBuilder.Create<FileNameValidationJob>()
                            .WithIdentity("File Name Validation", "Rules")
                            .Build);
                        q.AddTrigger(() => TriggerBuilder.Create()
                            .StartNow()
                            .WithCronSchedule(validationSchedule)
                            .Build());
                    });
                });

                hostConfig.StartAutomaticallyDelayed();

                // TODO: When the service is installed, the installer will prompt for the username/password combination used to launch the service.
                //hostConfig.RunAsPrompt();
                hostConfig.RunAsNetworkService();
            });
        }
    }
}
