﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Extras.Quartz;
using Raido.FilenameWatcher.AutofacLog4Net;
using Raido.FilenameWatcher.Domain.Jobs;
using Raido.FilenameWatcher.Domain.RulesEngine;
using Raido.FilenameWatcher.RuleConfiguration;
using Raido.FilenameWatcher.Service;

namespace Raido.FilenameWatcher
{
    public static class IoCBinding
    {
        public static IContainer GetIoCContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new LoggingModule());

            // Scan an assembly for components
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(IRulesProcessor)))
                                       .Where(t =>
                                                t.Namespace != null &&
                                                (
                                                    t.Namespace.StartsWith("Raido.FilenameWatcher.Domain.RulesEngine", StringComparison.Ordinal) ||
                                                    t.Namespace.StartsWith("Raido.FilenameWatcher.Domain.Validation", StringComparison.Ordinal) ||
                                                    t.Namespace.StartsWith("Raido.FilenameWatcher.Domain.Providers", StringComparison.Ordinal) ||
                                                    t.Namespace.StartsWith("Raido.FilenameWatcher.Domain.Mailer", StringComparison.Ordinal)
                                                ))
                                       .AsImplementedInterfaces()
                                       .SingleInstance();
            var config = RuleConfigurationSection.GetConfig();
            builder.Register((context) => config).InstancePerLifetimeScope();
            builder.RegisterType<FilenameWatcherService>().AsImplementedInterfaces().InstancePerLifetimeScope();


            // configure and register Quartz
            var schedulerConfig = new NameValueCollection {
                {"quartz.threadPool.threadCount", "3"},
                {"quartz.threadPool.threadNamePrefix", "SchedulerWorker"},
                {"quartz.scheduler.threadName", "Scheduler"}
            };

            builder.RegisterModule(new QuartzAutofacFactoryModule
            {
                ConfigurationProvider = c => schedulerConfig
            });
            
            builder.RegisterModule(new QuartzAutofacJobsModule(typeof(FileNameValidationJob).Assembly));
            return builder.Build();
        }
    }
}
