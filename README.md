# Summary #
This project arose for a friend that wanted a simple method to enforce certain naming convensions for documents saved onto shared folders

### Is there a working version hosted somewhere? ###
No. This is a windows service that must be installed onto a box and the cron job will run in the background

### What does it do? ###
The windows service will schedule a cron job via Quartz.Net to run validation. The folders that the system validates is configured in the application config.
the rules to which it should use on the folder is also configured in the application config.

### How do I get set up? ###
* Pull Latest
* Build Solution
* Update the rules settings with the folders and rules


### Example Rule Settings ###
Note that "schedule" is a cron. read more about it on the [quartz scheduler documentation](http://www.quartz-scheduler.net/documentation/quartz-2.x/tutorial/crontrigger.html)
The example below configures 2 folders to be validated.

* First folder will only log errors and has 3 ruls configured as "Rule1 && (Rule2 || Rule3)"
* Second folder will send an email with logged details (note that system.net mailsettings must be configured to do this). It also has 3 rules setup in the same format of "Rule1 && (Rule2 || Rule3)"

```xml
  <RuleSettings emailSubject="Filename Validation Results" toAddress="myaddress@domain.com" schedule="0 * * * * ?">
    <folder location="D:\Formula 1\" resultTarget="Log">
      <rule type="And">
        <rule type="Regex" value="^[A-Z]" />
        <rule type="Or">
          <rule type="Regex" value=".*[0-9]$" />
          <rule type="Regex" value=".*[0-9]backup$" />
        </rule>
      </rule>
    </folder>
    <folder location="D:\Car\" resultTarget="Email">
      <rule type="And">
        <rule type="Regex" value="^[A-Z]" />
        <rule type="Or">
          <rule type="Regex" value=".*[0-9].*$" />
          <rule type="Regex" value=".*########$" />
        </rule>
      </rule>
    </folder>
  </RuleSettings>
```

### Contribution guidelines ###
* Currently no contributions needed.

### Who do I talk to? ###
* Repo owner or admin