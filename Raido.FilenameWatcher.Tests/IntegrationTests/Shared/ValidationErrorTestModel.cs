﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.Tests.IntegrationTests.Shared
{
    internal sealed class ValidationErrorTestModel
    {
        public string FileName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
