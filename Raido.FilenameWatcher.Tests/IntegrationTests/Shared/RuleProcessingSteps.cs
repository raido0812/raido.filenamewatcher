﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Raido.FilenameWatcher.Domain.RulesEngine;
using Raido.FilenameWatcher.Domain.Validation;
using Raido.FilenameWatcher.Domain.Validation.Regex;
using Raido.FilenameWatcher.RuleConfiguration;
using Raido.FilenameWatcher.RuleConfiguration.Enums;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
namespace Raido.FilenameWatcher.Tests.IntegrationTests.Shared
{
    [Binding]
    internal class RuleProcessingSteps
    {
        internal const string FileNameListKey = "FileNameList";
        internal const string ValidationErrorsKey = "ValidationErrors";
        internal const string DefaultFolderName = "SomeFolderName";

        #region Given
        [Given(@"The following filenames are available")]
        public void GivenTheFollowingFilenamesAreAvailable(Table table)
        {
            var listOfNames = table.CreateSet<FileNameTestModel>();
            ScenarioContext.Current.Add(FileNameListKey, listOfNames);
        }

        #endregion

        #region When

        [When(@"I validate against the following rule configuration")]
        public void WhenIValidateAgainstTheFollowingRules(RuleConfigurationSection rules)
        {
            var regexValidator = new RegexValidator();
            var processor = new FileRulesProcessor(null, regexValidator);

            var fileNames = (ScenarioContext.Current[FileNameListKey] as IEnumerable<FileNameTestModel>)
                                .Select(fnt=>fnt.Filename);

            var validationErrors = processor.ProcessFileNames(fileNames, DefaultFolderName, rules.Folders[0].Rules, ResultTarget.None);

            ScenarioContext.Current.Add(ValidationErrorsKey, validationErrors);
        }

        [When(@"I validate against the regex rule ""(.*)""")]
        public void WhenIValidateAgainstTheRegexRule(string regex)
        {
            Rule rule = new Rule
            {
                Type = ElementType.Regex,
                RuleValue = regex,
            };
            var regexValidator = new RegexValidator();
            var processor = new FileRulesProcessor(null, regexValidator);

            var fileNames = ScenarioContext.Current[FileNameListKey] as IEnumerable<FileNameTestModel>;
            var validationErrors = new List<ValidationError>();
            foreach (var fileName in fileNames)
            {
                validationErrors.AddRange(processor.ProcessRule(rule, fileName.Filename, DefaultFolderName, ResultTarget.None));
            }

            ScenarioContext.Current.Add(ValidationErrorsKey, validationErrors);
        }

        [When(@"I validate against the following regex rules")]
        public void WhenIValidateAgainstTheFollowingRules()
        {
            ScenarioContext.Current.Pending();
        }

        #endregion

        #region Then
        [Then(@"The following validation Errors should be present")]
        public void ThenTheFollowingValidationErrorsShouldBePresent(Table table)
        {
            var listOfErrorMessages = table.CreateSet<ValidationErrorTestModel>().ToList();
            var validationErrors = ScenarioContext.Current[ValidationErrorsKey] as List<ValidationError>;

            Assert.AreEqual(listOfErrorMessages.Count, validationErrors.Count, "Message Count Different");
            foreach (var errorMessage in listOfErrorMessages)
            {
                Assert.IsTrue(validationErrors.Any(ve =>
                    ve.FileName.Equals(errorMessage.FileName, System.StringComparison.CurrentCultureIgnoreCase) &&
                    ve.Message.Equals(errorMessage.ErrorMessage, System.StringComparison.CurrentCultureIgnoreCase)),
                    $"Error Message \"{errorMessage.ErrorMessage}\" for File \"{errorMessage.FileName}\" not found");
            }
        }
        #endregion
    }
}
