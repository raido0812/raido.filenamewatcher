﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.Tests.IntegrationTests.Shared
{
    internal sealed class FileNameTestModel
    {
        public string Filename { get; set; }
    }
}
