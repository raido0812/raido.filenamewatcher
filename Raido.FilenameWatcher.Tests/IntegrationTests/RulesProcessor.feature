﻿Feature: Rules Processor
	In order to validate filenames against rules
    as a rules engine
    filenames must be validated according to rules specified


Scenario: File names with yyyy-mm-dd is valid
	Given The following filenames are available
    | Filename                                 |
    | 2016-10-17 Quote - ABC.pdf               |
    | Invoice - DEF - 2016-10-17 - Renewal.pdf |
	When I validate against the regex rule "\d\d\d\d-\d\d-\d\d"
	Then The following validation Errors should be present
    | Filename | ErrorMessage |
    
Scenario: File names without yyyy-mm-dd is invalid
	Given The following filenames are available
    | Filename                     |
    | Quote - ABC - 2016-10-08.pdf |
    | Invoice - DEF - Renewal.pdf  |
	When I validate against the regex rule "\d\d\d\d-\d\d-\d\d"
	Then The following validation Errors should be present
    | Filename                    | ErrorMessage                               |
    | Invoice - DEF - Renewal.pdf | Does not match pattern: \d\d\d\d-\d\d-\d\d |

Scenario: Rules with OR should give validation error if neither rules are met
    Given The following filenames are available
    | Filename                     |
    | Quote - ABC - 2016-10-08.pdf |
    | Invoice - DEF - Renewal.pdf  |
    When I validate against the following rule configuration
    """
    <RuleSettings emailSubject="Filename Validation Results" toAddress="kuannienlu@gmail.com">
        <folder location="RandomFolderName" resultTarget="Log">
            <rule type="Or">
                <rule type="Regex" value=".*[0-9].doc$" />
                <rule type="Regex" value=".*Renewal.doc$" />
            </rule>
        </folder>
    </RuleSettings>
    """
	Then The following validation Errors should be present
    | Filename                     | ErrorMessage                           |
    | Quote - ABC - 2016-10-08.pdf | Does not match pattern: .*[0-9].doc$   |
    | Quote - ABC - 2016-10-08.pdf | Does not match pattern: .*Renewal.doc$ |
    | Invoice - DEF - Renewal.pdf  | Does not match pattern: .*[0-9].doc$   |
    | Invoice - DEF - Renewal.pdf  | Does not match pattern: .*Renewal.doc$ |

	
Scenario: Rules with AND should give validation error if either rules are not met
    Given The following filenames are available
    | Filename                         |
    | 5 - Quote - ABC - 2016-10-08.doc |
    | Invoice - DEF - Renewal.doc      |
    When I validate against the following rule configuration
    """
    <RuleSettings emailSubject="Filename Validation Results" toAddress="kuannienlu@gmail.com">
        <folder location="RandomFolderName" resultTarget="Log">
            <rule type="And">
                <rule type="Regex" value="[0-9].*.doc$" />
                <rule type="Regex" value=".*Renewal.doc$" />
            </rule>
        </folder>
    </RuleSettings>
    """
	Then The following validation Errors should be present
    | Filename                         | ErrorMessage                           |
    | 5 - Quote - ABC - 2016-10-08.doc | Does not match pattern: .*Renewal.doc$ |
    | Invoice - DEF - Renewal.doc      | Does not match pattern: [0-9].*.doc$   |

	
Scenario: Combo Rules with nested Or statement inside AND
    Given The following filenames are available
    | Filename                         |
    | 5 - Quote - ABC - 2016-10-08.doc |
    | 4 - DEF - Renewal.doc      |
    When I validate against the following rule configuration
    """
    <RuleSettings emailSubject="Filename Validation Results" toAddress="kuannienlu@gmail.com">
        <folder location="RandomFolderName" resultTarget="Log">
            <rule type="And">
                <rule type="Regex" value="[0-9].*.doc$" />
				<rule type="Or">
					<rule type="Regex" value="Quote" />
					<rule type="Regex" value="Invoice" />
				</rule>
            </rule>
        </folder>
    </RuleSettings>
    """
	Then The following validation Errors should be present
    | Filename              | ErrorMessage                    |
    | 4 - DEF - Renewal.doc | Does not match pattern: Invoice |
    | 4 - DEF - Renewal.doc | Does not match pattern: Quote   |