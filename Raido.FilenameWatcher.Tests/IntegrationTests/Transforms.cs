﻿using Raido.FilenameWatcher.RuleConfiguration;
using TechTalk.SpecFlow;

namespace Raido.FilenameWatcher.Tests.IntegrationTests
{
    [Binding]
    internal sealed class Transforms
    {
        [StepArgumentTransformation]
        public RuleConfigurationSection CustomConfigSectionTransform(string xml)
        {
            return RuleConfigurationSection.GetConfigFromXml(xml);
        }
    }
}
