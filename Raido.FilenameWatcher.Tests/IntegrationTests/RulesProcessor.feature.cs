﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.1.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Raido.FilenameWatcher.Tests.IntegrationTests
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Rules Processor")]
    public partial class RulesProcessorFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "RulesProcessor.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Rules Processor", "\tIn order to validate filenames against rules\r\n    as a rules engine\r\n    filenam" +
                    "es must be validated according to rules specified", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("File names with yyyy-mm-dd is valid")]
        public virtual void FileNamesWithYyyy_Mm_DdIsValid()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("File names with yyyy-mm-dd is valid", ((string[])(null)));
#line 7
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename"});
            table1.AddRow(new string[] {
                        "2016-10-17 Quote - ABC.pdf"});
            table1.AddRow(new string[] {
                        "Invoice - DEF - 2016-10-17 - Renewal.pdf"});
#line 8
 testRunner.Given("The following filenames are available", ((string)(null)), table1, "Given ");
#line 12
 testRunner.When("I validate against the regex rule \"\\d\\d\\d\\d-\\d\\d-\\d\\d\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename",
                        "ErrorMessage"});
#line 13
 testRunner.Then("The following validation Errors should be present", ((string)(null)), table2, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("File names without yyyy-mm-dd is invalid")]
        public virtual void FileNamesWithoutYyyy_Mm_DdIsInvalid()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("File names without yyyy-mm-dd is invalid", ((string[])(null)));
#line 16
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename"});
            table3.AddRow(new string[] {
                        "Quote - ABC - 2016-10-08.pdf"});
            table3.AddRow(new string[] {
                        "Invoice - DEF - Renewal.pdf"});
#line 17
 testRunner.Given("The following filenames are available", ((string)(null)), table3, "Given ");
#line 21
 testRunner.When("I validate against the regex rule \"\\d\\d\\d\\d-\\d\\d-\\d\\d\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename",
                        "ErrorMessage"});
            table4.AddRow(new string[] {
                        "Invoice - DEF - Renewal.pdf",
                        "Does not match pattern: \\d\\d\\d\\d-\\d\\d-\\d\\d"});
#line 22
 testRunner.Then("The following validation Errors should be present", ((string)(null)), table4, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Rules with OR should give validation error if neither rules are met")]
        public virtual void RulesWithORShouldGiveValidationErrorIfNeitherRulesAreMet()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Rules with OR should give validation error if neither rules are met", ((string[])(null)));
#line 26
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename"});
            table5.AddRow(new string[] {
                        "Quote - ABC - 2016-10-08.pdf"});
            table5.AddRow(new string[] {
                        "Invoice - DEF - Renewal.pdf"});
#line 27
    testRunner.Given("The following filenames are available", ((string)(null)), table5, "Given ");
#line hidden
#line 31
    testRunner.When("I validate against the following rule configuration", @"<RuleSettings emailSubject=""Filename Validation Results"" toAddress=""kuannienlu@gmail.com"">
    <folder location=""RandomFolderName"" resultTarget=""Log"">
        <rule type=""Or"">
            <rule type=""Regex"" value="".*[0-9].doc$"" />
            <rule type=""Regex"" value="".*Renewal.doc$"" />
        </rule>
    </folder>
</RuleSettings>", ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename",
                        "ErrorMessage"});
            table6.AddRow(new string[] {
                        "Quote - ABC - 2016-10-08.pdf",
                        "Does not match pattern: .*[0-9].doc$"});
            table6.AddRow(new string[] {
                        "Quote - ABC - 2016-10-08.pdf",
                        "Does not match pattern: .*Renewal.doc$"});
            table6.AddRow(new string[] {
                        "Invoice - DEF - Renewal.pdf",
                        "Does not match pattern: .*[0-9].doc$"});
            table6.AddRow(new string[] {
                        "Invoice - DEF - Renewal.pdf",
                        "Does not match pattern: .*Renewal.doc$"});
#line 42
 testRunner.Then("The following validation Errors should be present", ((string)(null)), table6, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Rules with AND should give validation error if either rules are not met")]
        public virtual void RulesWithANDShouldGiveValidationErrorIfEitherRulesAreNotMet()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Rules with AND should give validation error if either rules are not met", ((string[])(null)));
#line 50
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename"});
            table7.AddRow(new string[] {
                        "5 - Quote - ABC - 2016-10-08.doc"});
            table7.AddRow(new string[] {
                        "Invoice - DEF - Renewal.doc"});
#line 51
    testRunner.Given("The following filenames are available", ((string)(null)), table7, "Given ");
#line hidden
#line 55
    testRunner.When("I validate against the following rule configuration", @"<RuleSettings emailSubject=""Filename Validation Results"" toAddress=""kuannienlu@gmail.com"">
    <folder location=""RandomFolderName"" resultTarget=""Log"">
        <rule type=""And"">
            <rule type=""Regex"" value=""[0-9].*.doc$"" />
            <rule type=""Regex"" value="".*Renewal.doc$"" />
        </rule>
    </folder>
</RuleSettings>", ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename",
                        "ErrorMessage"});
            table8.AddRow(new string[] {
                        "5 - Quote - ABC - 2016-10-08.doc",
                        "Does not match pattern: .*Renewal.doc$"});
            table8.AddRow(new string[] {
                        "Invoice - DEF - Renewal.doc",
                        "Does not match pattern: [0-9].*.doc$"});
#line 66
 testRunner.Then("The following validation Errors should be present", ((string)(null)), table8, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Combo Rules with nested Or statement inside AND")]
        public virtual void ComboRulesWithNestedOrStatementInsideAND()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Combo Rules with nested Or statement inside AND", ((string[])(null)));
#line 72
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table9 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename"});
            table9.AddRow(new string[] {
                        "5 - Quote - ABC - 2016-10-08.doc"});
            table9.AddRow(new string[] {
                        "4 - DEF - Renewal.doc"});
#line 73
    testRunner.Given("The following filenames are available", ((string)(null)), table9, "Given ");
#line hidden
#line 77
    testRunner.When("I validate against the following rule configuration", @"<RuleSettings emailSubject=""Filename Validation Results"" toAddress=""kuannienlu@gmail.com"">
    <folder location=""RandomFolderName"" resultTarget=""Log"">
        <rule type=""And"">
            <rule type=""Regex"" value=""[0-9].*.doc$"" />
<rule type=""Or"">
	<rule type=""Regex"" value=""Quote"" />
	<rule type=""Regex"" value=""Invoice"" />
</rule>
        </rule>
    </folder>
</RuleSettings>", ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table10 = new TechTalk.SpecFlow.Table(new string[] {
                        "Filename",
                        "ErrorMessage"});
            table10.AddRow(new string[] {
                        "4 - DEF - Renewal.doc",
                        "Does not match pattern: Invoice"});
            table10.AddRow(new string[] {
                        "4 - DEF - Renewal.doc",
                        "Does not match pattern: Quote"});
#line 91
 testRunner.Then("The following validation Errors should be present", ((string)(null)), table10, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
