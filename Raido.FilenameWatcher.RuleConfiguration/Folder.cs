﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.RuleConfiguration
{
    public sealed class Folder : ConfigurationElement
    {
        [ConfigurationProperty("location", IsRequired = true)]
        public string Location
        {
            get { return this["location"] as string; }
            set { this["location"] = value; }
        }

        [ConfigurationProperty("resultTarget", IsRequired = true, DefaultValue = ResultTarget.None)]
        public ResultTarget ResultTarget
        {
            get { return (ResultTarget)this["resultTarget"]; }
            set { this["resultTarget"] = value; }
        }

        internal string Key { get; }

        public Folder()
        {
            this.Key = Guid.NewGuid().ToString();
        }

        [ConfigurationProperty("", IsDefaultCollection = true, IsRequired = false)]
        public RuleCollection Rules
        {
            get
            {
                return base[""] as RuleCollection;
            }
        }
    }
}
