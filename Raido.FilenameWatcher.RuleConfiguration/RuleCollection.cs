﻿
using System.Configuration;
using System.IO;

namespace Raido.FilenameWatcher.RuleConfiguration
{
    public sealed class RuleCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Rule();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Rule)element).Key;
        }

        protected override string ElementName
        {
            get { return "rule"; }
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        [ConfigurationProperty("path", IsRequired = true)]
        public string FolderPath
        {
            get
            {
                return this["path"] as string;
            }
            set
            {
                this["path"] = value;
            }
        }

        public Rule this[int idx]
        {
            get
            {
                return (Rule)BaseGet(idx);
            }
        }
    }
}
