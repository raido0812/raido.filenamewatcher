﻿using System.Configuration;

namespace Raido.FilenameWatcher.RuleConfiguration
{
    public sealed class FolderCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Folder();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Folder) element).Key;
        }

        protected override string ElementName
        {
            get { return "folder"; }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }
        
        public Folder this[int idx]
        {
            get { return (Folder) BaseGet(idx); }
        }
    }
}