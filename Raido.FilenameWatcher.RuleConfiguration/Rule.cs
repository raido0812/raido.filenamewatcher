﻿using System;
using System.Configuration;
using Raido.FilenameWatcher.RuleConfiguration.Enums;

namespace Raido.FilenameWatcher.RuleConfiguration
{
    public sealed class Rule : ConfigurationElement
    {
        [ConfigurationProperty("type", IsRequired = true)]
        public ElementType Type
        {
            get
            {
                return (ElementType)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }

        [ConfigurationProperty("value", IsRequired = false)]
        public string RuleValue
        {
            get
            {
                return this["value"] as string;
            }
            set
            {
                this["value"] = value;
            }
        }

        internal string Key { get; }

        public Rule()
        {
            this.Key = Guid.NewGuid().ToString();
        }

        [ConfigurationProperty("", IsDefaultCollection = true, IsRequired = false)]
        public RuleCollection Rules
        {
            get
            {
                return base[""] as RuleCollection;
            }
        }
    }
}
