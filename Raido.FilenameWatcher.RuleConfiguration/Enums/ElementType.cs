﻿namespace Raido.FilenameWatcher.RuleConfiguration.Enums
{
    public enum ElementType
    {
        Unknown = 0,
        And = 1,
        Or = 2,
        Regex = 4
    }
}
