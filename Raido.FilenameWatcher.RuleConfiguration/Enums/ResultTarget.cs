﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raido.FilenameWatcher.RuleConfiguration.Enums
{
    public enum ResultTarget
    {
        None = 0,
        Email = 1,
        Log = 2
    }
}
