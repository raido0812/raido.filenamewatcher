﻿using System.Configuration;
using System.IO;

namespace Raido.FilenameWatcher.RuleConfiguration
{

    public sealed class RuleConfigurationSection : ConfigurationSection
    {
        public static RuleConfigurationSection GetConfig()
        {
            return ConfigurationManager.GetSection("RuleSettings") as RuleConfigurationSection;
        }

        public static RuleConfigurationSection GetConfigFromXml(string xmlData)
        {
            var config = new RuleConfigurationSection();
            using (var textReader = new StringReader(xmlData))
            using (var xmlReader = System.Xml.XmlReader.Create(textReader))
            {
                config.DeserializeSection(xmlReader);
            }

            return config;
        }

        [ConfigurationProperty("schedule", IsRequired = false)]
        public string ScheduleCron
        {
            get { return this["schedule"] as string; }
            set { this["schedule"] = value; }
        }

        [ConfigurationProperty("toAddress", IsRequired = false)]
        public string ToAddress
        {
            get { return this["toAddress"] as string; }
            set { this["toAddress"] = value; }
        }

        [ConfigurationProperty("emailSubject", IsRequired = false)]
        public string EmailSubject
        {
            get { return this["emailSubject"] as string; }
            set { this["emailSubject"] = value; }
        }

        [ConfigurationProperty("", IsDefaultCollection = true, IsRequired = true)]
        [ConfigurationCollection(typeof(FolderCollection), AddItemName = "folder")]
        public FolderCollection Folders
        {
            get { return base[""] as FolderCollection; }
            set { base[""] = value; }
        }
    }
}
